import {NgModule, Component, ViewChild, AfterViewInit} from '@angular/core';
// import {HttpModule, Http, Response} from '@angular/http';
import {HttpClientModule, HttpClient} from '@angular/common/http';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs/Rx';
import { Http,Response,Headers } from '@angular/http';
import { LoginComponent } from './login/login.component';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  @ViewChild(LoginComponent) loginstatus;
  orderplace=[];
  color='white';
  login=true;
  //color2="black";
  glass=false;
  onesecond=60000;
  orderdetail;
  inqueue=0;
  avgpickuptime=20;
  avgdeliverytime=30;
  stores=[];
  loginapp(e){
    console.log('app login');
    console.log(e);
    this.login=e;    
  }

  constructor(private http: Http) {

    this.get('http://13.56.147.24:9030/all-outlet/nokey').map((res: Response) => res.json()).subscribe(outlet => {
          this.stores=outlet;
      });
    
  }
 selected_store={name:''};
  createAuthorizationHeader(headers: Headers) {
    if(this.selected_store.name!='')
    {
      headers.append('store', this.selected_store.name); 
    }
  }

  get(url) {
    let headers = new Headers();
    this.createAuthorizationHeader(headers);
    return this.http.get(url, {
      headers: headers
    });
  }

  afterselect_store(i)
  {
    this.selected_store = this.stores[i];
    this.avgpickuptime=20;
    this.avgdeliverytime=30;
    if(this.stores && this.stores[i].avgpickuptime)
    {
      this.avgpickuptime = this.stores[i].avgpickuptime;
    }
    if(this.stores && this.stores[i].avgdeltime)
    {
      this.avgdeliverytime = this.stores[i].avgdeltime;
    }
    this.constructorfunction();
    Observable.interval(250 * 60).subscribe(x => {
      console.log('interval')
      this.checkneworder();
    });
  }

  checkneworder()
  {
    let headers = new Headers();
    this.get('http://13.56.147.24:9030/admin-new-order-with-schedule-chef/nokey').map((res: Response) => res.json()).subscribe(
      data => {
          // console.log(data.length,this.totalorders)

          // for (let k = 0;k < this.all_orders.length; k++) {
          //   let index = data.findIndex(x => x.auto_id==this.orderplace[k].auto_id);
          //   // console.log('index in interval',index);
          //   if(index==-1)
          //   {
          //     this.orderplace.splice(k,1);
          //     this.totalorders--;
          //     for (let k = 0; k < this.all_orders.length; k++) {
          //       // console.log(this.all_orders[k].used);
          //       if(this.orderplace.length<8 && this.all_orders[k].used==undefined)
          //                   {
          //                     this.ordercheckfunction(this.all_orders[k],'shift');
          //                   } 
          //     }

          //   }else{
          //     // console.log('found');
          //   } 
          // }
          // for (let k = 0;k < this.all_orders.length; k++) {
          //   let index = data.findIndex(x => x.auto_id==this.all_orders[k].auto_id);
          //   // console.log('index in interval',index);
          //   if(index==-1)
          //   {
          //     let index_in_orderplace = this.orderplace.findIndex(x => x.auto_id==this.all_orders[k].auto_id);
          //     this.all_orders.splice(k,1);
          //     if(index_in_orderplace!=-1)
          //     {
          //       this.orderplace.splice(k,1);
          //     }              
          //     this.totalorders--;
          //     for (let k = 0; k < this.all_orders.length; k++) {
          //       // console.log(this.all_orders[k].used);
          //       if(this.orderplace.length<8 && this.all_orders[k].used==undefined)
          //                   {
          //                     this.all_orders[k].used=true;
          //                     this.ordercheckfunction(this.all_orders[k],'shift');
          //                   } 
          //     }

          //   }else{
          //     // console.log('found');
          //   } 
          // }
        console.log('this.all_orders',this.all_orders,this.orderplace);  
        console.log('data.length',data.length," ",this.totalorders);
          if (data && data.length < this.totalorders) {
            this.totalorders = data.length;
          }
          
          // if (data && data.length > this.totalorders) {
          //   console.log('yes');
          //   let difference = data.length-this.totalorders;
          //   this.totalorders+=difference;

            // console.log('difference',difference)
            // console.log('data.length-difference',data.length-difference);
            // console.log('data.length',data.length);
           
            /*comment on 31 july */
            for (let k = 0;k < data.length; k++) {
              // console.log('k',k);
              // console.log('data',data[k]);
                // this.orderplace.push(data[k]);
                let index_in_all_orders = this.all_orders.findIndex(x=>x.auto_id==data[k].auto_id);
                
                console.log(index_in_all_orders,'&&',data[k].chef_status);

                if(index_in_all_orders==-1 && (data[k].chef_status==false || data[k].chef_status==undefined)){
                  console.log('in push');
                  console.log(this.orderplace.length);
                  if(this.orderplace.length<8)
                    {
                      console.log('make it used');
                      this.ordercheckfunction(data[k],'shift');
                      data[k].used=true;
                    }

                this.all_orders.push(data[k]);
                console.log('this.all_orders',this.all_orders);
              }
                 // this.ordercheckfunction(data[k],'shift');           
             }  

             for (let index = 0; index < this.orderplace.length; index++) {
               let index_in_order = data.findIndex(x=>x.auto_id==this.orderplace[index].auto_id);
               this.orderplace[index].reasontoedit = data[index_in_order].reasontoedit;
             }
             /*comment on 31 july */           
          // }

          
          this.inqueue=0;
             for (let k = 0;k < this.all_orders.length; k++) {
               if(this.all_orders[k].used==undefined)
               {
                this.ordercheckfunction(this.all_orders[k],'inqueue');
                // this.all_orders[k].used=true;
               }
             }
          /*order-item*/
        });
  }
  
  ordercheckfunction(data,push_or_shift)
  {
    let index=-1;
    // console.log("data.auto_id-->",data.auto_id);
  if(data && data.session_cart!=undefined){
    for (let categories = 0; categories < this.categories_id_to_show.length; categories++) {
    
      index = data.session_cart.findIndex(x => x.category_name == this.categories_id_to_show[categories]);
      
      // console.log('indexs',index,data.session_cart, this.categories_id_to_show[categories]);
//       Beacon and wedged
// Browine
// Dessert pizza..

if(index!=-1 && index!=undefined)
      {
        if(data.session_cart[index].category_name=='5a1bd709b663f2e3cc093792' || data.session_cart[index].category_name=='5a1bd6cfb663f2e3cc093791')
        {
            let dish_matched =  data.session_cart[index].name.toLowerCase().includes('garlic bread') || data.session_cart[index].name.toLowerCase().includes('wings') ||
              data.session_cart[index].name.toString().toLowerCase().includes('herbed pesto pizza bread') || data.session_cart[index].name.toString().toLowerCase().includes('dessert pizza') || data.session_cart[index].name.toLowerCase().includes('brownie') || data.session_cart[index].name.toLowerCase().includes('beacon') || data.session_cart[index].name.toLowerCase().includes('wedged');
              if(dish_matched)
              {
                break;
              }else{
                index=undefined;
              }

        }else{
          break;
        }
      }
    }
  }

  // console.log('index',index,data.chef_status,index,data.used,push_or_shift);
  // console.log(index!=-1 && index!=undefined && !data.chef_status);
  // console.log(index,index!=-1 , index!=undefined , !data.chef_status);
    if(index!=-1 && index!=undefined && !data.chef_status)
    {
      data.timestarmp = (new Date(data.created_at)).getTime();
      if(push_or_shift=='inqueue'){
        this.inqueue++;
        return true;
      }else if(push_or_shift=='unshift'){
        // this.orderplace.unshift(data);
        this.orderplace.unshift(data);
        // console.log('shifted to orderplace');
      }else{
        this.orderplace.push(data);
        // console.log('pushed to orderplace');
      }


    setTimeout(() => {
      // this.glass=true;
        setTimeout(() => {
          // this.glass=false;
          let index_in_orderplace  = this.orderplace.findIndex(x => x._id == data['_id']);
          this.color="yellow";
          if(index_in_orderplace!=-1){
          this.orderplace[index_in_orderplace].color="yellow";
          // console.log(this.color);
            setTimeout(() => {
              // this.glass=true;
              // console.log(this.color);
              setTimeout(() => {
                // this.glass=false;
                this.color='red';
                let index_in_orderplace  = this.orderplace.findIndex(x => x._id == data['_id']);
                if(this.orderplace[index_in_orderplace]){

                  this.orderplace[index_in_orderplace].color="red";
                  this.orderplace[index_in_orderplace].color2="white";
                }

                // console.log(this.color);
              }, this.onesecond*1);
            }, this.onesecond*1);
          }
        }, this.onesecond*2);
    }, this.onesecond*1);
  }
    //timeout
  }

  categories_id_to_show=['5a06a0d59bc6801d36fda738','5a1bd787b663f2e3cc093795','5a72f00e0c50931f766c4e6e','5a7b1eca23b49e76af61d387','5a1bd6cfb663f2e3cc093791','5a1bd709b663f2e3cc093792'];

  totalorders=0;
  all_orders;
  
  constructorfunction()
  {
    this.get('http://13.56.147.24:9030/admin-new-order-with-schedule-chef/nokey').map((res: Response) => res.json()).subscribe(
    data2 => {
      this.orderplace=[];
      this.all_orders = data2;
      this.all_orders.reverse();
      for (let k = 0; k < this.all_orders.length; k++) {
          if(this.orderplace.length<8)
          {
            this.ordercheckfunction(this.all_orders[k],'push');
            this.all_orders[k].used=true;
          }
      }

      // console.log('total orders',data2.length);
      this.totalorders = data2.length;
      // console.log('this.orderplace.length',this.orderplace.length);
      this.color='white';
      console.log('this.orderplace',this.orderplace);
      console.log('this.all_orders',this.all_orders);
      this.checkneworder();
    });
  }

  close(indx)
  {
     console.log('food-ready-close');
    this.get('http://13.56.147.24:9030/api/chef_order_update/'+this.orderplace[indx]._id+'/nokey').map((res: Response) => res.json()).subscribe(data=>{
      // console.log(data);
      // console.log(this.orderplace);
      // console.log(this.orderplace[indx]);
      let order_auto_id = this.orderplace[indx].auto_id; 
      this.orderplace.splice(indx,1);
      
     
      // this.totalorders--;
      //new code
      console.log('this.all_orders',this.all_orders);
      // for (let k = 0; k<=this.all_orders.length-1; k++) {
      //     //const element = data[k];
      //      console.log(this.all_orders[k].auto_id,this.all_orders[k].used,this.orderplace.length<8 && this.all_orders[k].used==undefined);
      //     if(this.orderplace.length<8 && this.all_orders[k].used==undefined)
      //                 {
      //                   this.all_orders[k].used=true;
      //                   this.ordercheckfunction(this.all_orders[k],'shift');

      //                   if(this.inqueue>0)
      //                   {
      //                     this.inqueue--;
      //                   }
      //                 }
                      
      //     if(order_auto_id==this.all_orders[k].auto_id)
      //     {
      //       // this.all_orders.splice(k,1);
      //       // this.totalorders--;
      //       // this.orderplace.splice(indx,1);
            
      //     }             
      //   }
        
        for (let k = 0; k<=this.all_orders.length-1; k++) {
        //const element = data[k];

        console.log(this.all_orders[k].used);
        if(this.orderplace.length<8 && this.all_orders[k].used==undefined)
                    {
                      this.all_orders[k].used=true;
                      this.ordercheckfunction(this.all_orders[k],'shift');
                      if(this.inqueue>0)
                      {
                        this.inqueue--;
                      }
                    }
                    
        if(order_auto_id==this.all_orders[k].auto_id)
        {
          // this.all_orders.splice(k,1);
          // this.totalorders--;
          // this.orderplace.splice(indx,1);
          
        }             
      }
                                   
      this.checkneworder();          
      //new code

      // console.log(this.orderplace);
      // this.constructorfunction();
    });
  }

  skip(indx)
  {
    // console.log('food-ready-close');
    //this.get('http://13.56.147.24:9030/api/chef_order_update/'+this.orderplace[indx]._id+'/nokey').map((res: Response) => res.json()).subscribe(data=>{
      // console.log(data);
      // console.log(this.orderplace);
      // console.log(this.orderplace[indx]);

      this.orderplace.push(this.orderplace[indx]);
      this.orderplace.splice(indx,1);
      // console.log(this.orderplace);
      // this.constructorfunction();
    // });
  }

  reopen(order)
  {
    console.log('reopen');
    this.get('http://13.56.147.24:9030/api/reopen_order_update/'+order._id+'/nokey').map((res: Response) => res.json()).subscribe(data=>{
      console.log(data);
      order.reopen_status=true;
      order.chef_status=false;
      order.used=true;
      this.all_orders.push(order);
      this.ordercheckfunction(order,'unshift');
      (<HTMLInputElement>document.getElementById('reopen_input')).value='';
      this.orderdetail=undefined;
      // console.log(this.orderplace);
      // console.log(this.orderplace[indx]);
      // console.log(this.orderplace);
      // this.constructorfunction();
    });
  }

  search_order(orderid)
  {
    console.log('llll'+orderid+'lll');
    if(orderid.length>0 && orderid!='' && orderid!=0){
      this.get('http://13.56.147.24:9030/order-detail-auto-id/'+orderid+'/nokey').map((res: Response) => res.json()).subscribe(data=>{
        console.log(data);
        this.orderdetail=data[0];
        // this.constructorfunction();
      });
    }else{
      this.orderdetail=undefined;
    }
      // setTimeout(() => {
      //   this.orderdetail=undefined;
      // },500);
  }
}
