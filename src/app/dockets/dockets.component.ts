import {NgModule, Component} from '@angular/core';
// import {HttpModule, Http, Response} from '@angular/http';
import {HttpClientModule, HttpClient} from '@angular/common/http';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs/Rx';
import { Http,Response } from '@angular/http';

@Component({
  selector: 'app-dockets',
  templateUrl: './dockets.component.html',
  styleUrls: ['./dockets.component.css']
})

export class DocketsComponent {
  orderplace=[];
  color='white';
  //color2="black";
  glass=false;
  onesecond=6000;
  orderdetail;
  constructor(private http: Http) {

    this.constructorfunction();
    Observable.interval(250 * 60).subscribe(x => {
      console.log('interval')
      this.http.get('http://13.56.147.24:9030/admin-new-order-with-schedule/nokey').map((res: Response) => res.json()).subscribe(
      data => {
          console.log(data.length,this.totalorders)
          if (data && data.length > this.totalorders) {
            console.log('yes');
            let difference = data.length-this.totalorders;
            this.totalorders+=difference;
            // console.log('difference',difference)
            // console.log('data.length-difference',data.length-difference);
            // console.log('data.length',data.length);
            for (let k = 0;k < difference; k++) {
              // console.log('k',k);
              // console.log('data[k]',data[k]);
                // this.orderplace.push(data[k]);
                let index=-1;
              // for (let session_cart_items = 0; session_cart_items < data2[k].session_cart.length; session_cart_items++) {
              if(data[k].session_cart!=undefined){
                for (let categories = 0; categories < this.categories_id_to_show.length; categories++) {
                  index = data[k].session_cart.findIndex(x => x.category_name == this.categories_id_to_show[categories]);
                  if(index!=-1 && index!=undefined)
                  {
                    console.log('matched');
                    break;
                  }
                }
              }
                if(index!=-1 && index!=undefined && !data[k].chef_status)
                {
                this.orderplace.unshift(data[k]);

                // console.log('this.orderplace',this.orderplace);
                // console.log(data[k]['_id']);

                // console.log(index_in_orderplace);
                //timeout
                // if(index_in_orderplace!=-1){
                setTimeout(() => {
                  // this.glass=true;
                    setTimeout(() => {
                      // this.glass=false;
                      let index_in_orderplace  = this.orderplace.findIndex(x => x._id == data[k]['_id']);
                      this.color="yellow";
                      this.orderplace[index_in_orderplace].color="yellow";
                      console.log(this.color);
                        setTimeout(() => {
                          // this.glass=true;
                          console.log(this.color);
                          setTimeout(() => {
                            // this.glass=false;
                            this.color='red';
                            let index_in_orderplace  = this.orderplace.findIndex(x => x._id == data[k]['_id']);
                            if(this.orderplace[index_in_orderplace]){

                              this.orderplace[index_in_orderplace].color="red";
                              this.orderplace[index_in_orderplace].color2="white";
                            }

                            console.log(this.color);
                          }, this.onesecond*1);
                        }, this.onesecond*1);
                    }, this.onesecond*2);
                }, this.onesecond*1);
              }
                //timeout
             }
          }
          /*order-item*/
        });
    });
  }
  categories_id_to_show=['5a06a0d59bc6801d36fda738','5a1bd6cfb663f2e3cc093791'];
  totalorders=0;
  constructorfunction()
  {

    this.http.get('http://13.56.147.24:9030/admin-new-order-with-schedule/nokey').map((res: Response) => res.json()).subscribe(
    data2 => {
      this.orderplace=[];
      for (let k = 0; k < data2.length; k++) {

 console.log(data2[k]);
        let index=-1;
      // for (let session_cart_items = 0; session_cart_items < data2[k].session_cart.length; session_cart_items++) {
      if(data2[k].session_cart!=undefined){
        for (let categories = 0; categories < this.categories_id_to_show.length; categories++) {
          index = data2[k].session_cart.findIndex(x => x.category_name == this.categories_id_to_show[categories]);
          if(index!=-1 && index!=undefined)
          {
            // console.log('matched');
            break;
          }
        }
      }
        if(index!=-1 && index!=undefined && !data2[k].chef_status)
        {

          this.orderplace.push(data2[k]);


          //timeout
          setTimeout(() => {
            // this.glass=true;
              setTimeout(() => {
                // this.glass=false;
                let index_in_orderplace  = this.orderplace.findIndex(x => x._id == data2[k]['_id']);
                this.color="yellow";
                this.orderplace[index_in_orderplace].color="yellow";
                console.log(this.color);
                  setTimeout(() => {
                    // this.glass=true;
                    console.log(this.color);
                    setTimeout(() => {
                      // this.glass=false;
                      this.color='red';
                      let index_in_orderplace  = this.orderplace.findIndex(x => x._id == data2[k]['_id']);
                      if(this.orderplace[index_in_orderplace]){

                        this.orderplace[index_in_orderplace].color="red";
                        this.orderplace[index_in_orderplace].color2="white";
                      }

                      console.log(this.color);
                    }, this.onesecond*1);
                  }, this.onesecond*1);
              }, this.onesecond*2);
          }, this.onesecond*1);
          //timeout
        }
      }
        console.log('total orders',data2.length);
        this.totalorders = data2.length;
      console.log('this.orderplace.length',this.orderplace.length);
      this.color='white';
      console.log(this.orderplace);
    });


  }

  close(indx)
  {
    console.log('food-ready-close');
    this.http.get('http://13.56.147.24:9030/api/chef_order_update/'+this.orderplace[indx]._id+'/nokey').map((res: Response) => res.json()).subscribe(data=>{
      console.log(data);
      console.log(this.orderplace);
      console.log(this.orderplace[indx]);
      this.orderplace.splice(indx,1);
      console.log(this.orderplace);
      // this.constructorfunction();
    });
  }

  search_order(orderid)
  {
    console.log(orderid)
    if(orderid!=''){
      this.http.get('http://13.56.147.24:9030/order-detail-auto-id/'+orderid+'/nokey').map((res: Response) => res.json()).subscribe(data=>{
        console.log(data);
        this.orderdetail=data[0];
        // this.constructorfunction();
      });
    }else{
      this.orderdetail=undefined;
    }
      // setTimeout(() => {
      //   this.orderdetail=undefined;
      // },500);
  }
}
